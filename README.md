# ESP32 Bluetooth Example

I'll be trying to connect my ESP32 µC to my Android phone via Bluetooth - let's see how that works out ;)

## ESP 32 Setup

Install everything according to the [Official Documentation](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html)

Note: Run the following commands using the ESP-IDF CMD in `${repo}/esp` to build the program:

```
idf.py set-target esp32
idf.py build
```

To flash the program onto the chip/eval kit you can use:

```
idf.py -p (PORT) flash
```

And to check output you can use:

```
idf.py -p (PORT) monitor
```