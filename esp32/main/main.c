#include <stdio.h>
#include "esp_err.h"
#include "nvs_flash.h"

void app_main(void) {
    printf("[TRACE] Bluetooth Example Start\n");
    printf("[TRACE] Initializing Non-Volatile Storage\n");
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_OK) {
        printf("[TRACE] Non-volatile storage was successfully initialized\n");
    }
    else if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        printf("[TRACE] NVS partition doesn't contain any empty pages. This may happen if NVS partition was truncated. Erase the whole partition and call nvs_flash_init again.\n");
    }
    else if (ret == ESP_ERR_NOT_FOUND) {
        printf("[TRACE] No partition with label \"nvs\" was found in the partition table\n");
    }
    else if (ret == ESP_ERR_NO_MEM) {
        printf("[TRACE] Memory could not be allocated for initializing NVS\n");
    }
}